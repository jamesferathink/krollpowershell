﻿Set-ExecutionPolicy Bypass -Scope Process -Force; 

function Install-Docker { 

    Write-Host Installing Docker...

    #PowerShell -NoProfile -ExecutionPolicy Bypass -Command "& {Start-Process PowerShell  -ArgumentList 'Set-ExecutionPolicy RemoteSigned' -Verb RunAs}"

    Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force

    Install-Module -Name DockerMsftProvider -Repository PSGallery -Force

    Install-Package -Name docker -ProviderName DockerMsftProvider

    Stop-Service docker    dockerd --unregister-service    dockerd -H npipe:// -H 0.0.0.0:2375 --register-service
}

function Install-Choco {
    Write-Host Installing Chocolatey...

    iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}

function Install-Support {

    Write-Host Installing Iguana...

    choco install -y iguana --version 6.1.2
}

try {    

    Install-Docker
    Install-Choco
    Install-Support

    Write-Host You must restart the machine...
    Write-Host

    $restart = Read-Host -Prompt 'Hit ''y'' to restart'
    
    if($restart -eq 'y'){
        Write-Hosting Restarting...
        Restart-Computer -Force
    }


} catch { 
    Write-Host Installation failed.
}